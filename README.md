# Libft, by mlaneyri
My very first own library !
This bad boy got me a 115/100.
## Usage
`make` for the mandatory functions, `make bonus` for the bonus part.
## Concerning the norm
This project was originally made to comply to the version 2.0.2 of the norm.
It was then updated to conform to norm v3. However, the original version is saved in branch `normv2`.
