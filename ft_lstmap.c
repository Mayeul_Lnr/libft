/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/19 23:47:01 by mlaneyri          #+#    #+#             */
/*   Updated: 2020/12/31 00:19:04 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_r(t_list *l1, t_list **l2, void *(*f)(void *), void (*d)(void *))
{
	void	*new_content;

	if (!l1)
		return (0);
	new_content = f(l1->content);
	if (!new_content)
		return (-1);
	*l2 = ft_lstnew(new_content);
	if (!*l2)
	{
		free(new_content);
		return (-1);
	}
	if (!ft_r(l1->next, &((*l2)->next), f, d))
		return (0);
	else
	{
		ft_lstdelone(*l2, d);
		return (-1);
	}
}

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list	*ret;

	ret = NULL;
	if (!ft_r(lst, &ret, f, del))
		return (ret);
	else
		return (NULL);
}
