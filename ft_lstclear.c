/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstclear.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/19 23:10:23 by mlaneyri          #+#    #+#             */
/*   Updated: 2020/11/20 15:15:13 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstclear(t_list **lst, void (*del)(void *))
{
	t_list	*cpy;

	if (!*lst)
		return ;
	cpy = (*lst)->next;
	if ((*lst)->content)
		del((*lst)->content);
	free(*lst);
	*lst = NULL;
	if (cpy)
		ft_lstclear(&cpy, del);
}
